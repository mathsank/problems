struct Node
{
    int data;
    struct Node *next;
};
class Llist
{    struct Node *head,*sorted;
    
public:
    Llist();
    void setHead(struct Node* headV);
    struct Node* getHead();
    void display();
    Node* createNode(int value);
    void insertBegining(int value);
    int length();
    int count(int value);
    void reverse(struct Node* headR);
    struct Node* reverseK(struct Node* headR, int k);
};
