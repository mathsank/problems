#include<iostream>
#include "llrevk.h"
using namespace std;

Llist:: Llist()
{ head = NULL;
    sorted = NULL;
}
struct Node* Llist:: getHead()
{ return head;
}
void Llist:: setHead(struct Node* headV)
{ head = headV;
}

void Llist:: display()
{
    struct Node *temp;
    if(head == NULL)
    {
        cout<<"The List is Empty"<<endl;
        return;
    }
    temp = head;
    cout<<"Elements of list are: "<<endl;
    while (temp != NULL)
    {
        cout<<temp->data<<"->";
        temp = temp->next;
    }
    cout<<"NULL"<<endl;
}
Node* Llist:: createNode(int value)
{
    struct Node *temp;
    temp = new(struct Node);
    if (temp == NULL)
    {
        cout<<"Memory not allocated "<<endl;
        return 0;
    }
    else
    {
        temp->data = value;
        temp->next = NULL;
        return temp;
    }
}
void Llist:: insertBegining(int value)
{   struct Node* temp;
    temp = createNode(value);
    struct Node* top;
    if (head == NULL)
    {
        head = temp;
        head->next = NULL;
    }
    else
    {
        top = head;
        head = temp;
        head->next = top;
    }
    cout<<"Element Inserted at beginning"<<endl;
}
int Llist:: length()
{
    int count = 0;
    struct Node *current = head;
    while (current != NULL)
    {
        count++;
        current = current->next;
    }
    return count;
}
int Llist:: count(int value)
{
    struct Node* current = head;
    int count = 0;
    while (current != NULL)
    {
        if(current->data == value)
            count++;
        current = current->next;
    }
    return count;
}

struct Node* Llist:: reverseK(struct Node* headR, int k)
{
    struct Node* current = headR;
    struct Node* next = NULL;
    struct Node* prev = NULL;
    int count = 0;
    while (current != NULL && count < k)        //reverses first k nodes
    {
        next  = current->next;
        current->next = prev;
        prev = current;
        current = next;
        count++;
    }
    if (next !=  NULL)                          //next points to k+1 node and recursively reverse next k nodes
        headR->next = reverseK(next, k);        //make rest of list as next of first node
    
     return prev;                               //new head of the reversed list/sublist
}

int main()
{
    Llist list1;
    struct Node* headR;
    list1.insertBegining(8);
    list1.insertBegining(7);
    list1.insertBegining(6);
    list1.insertBegining(5);
    list1.insertBegining(4);
    list1.insertBegining(3);
    list1.insertBegining(2);
    list1.insertBegining(1);
    list1.display();
    headR = list1.getHead();
    headR = list1.reverseK(headR, 4);
    list1.setHead(headR);
    list1.display();
    
    return 0;
}

